angular.module('app')
    .constant('FirebaseUrl', 'https://hack-expense-tracker.firebaseio.com')
    .service('rootRef', ['FirebaseUrl', Firebase]);

